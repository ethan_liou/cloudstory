//
//  DetailViewController.m
//  CloudStory
//
//  Created by Ethan on 2015/8/26.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import "DetailViewController.h"
#import "MediaObject.h"
#import <SVProgressHUD.h>
@import MediaPlayer;

@interface DetailViewController ()

@property (nonatomic, strong) MPMoviePlayerController * player;

@end

@implementation DetailViewController

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex != alertView.cancelButtonIndex){
        [[NSFileManager defaultManager] removeItemAtPath:_obj.fileUrl error:nil];
        [MediaObject LoadMediaFromLocal];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)share{
    NSURL *url = nil;
    if([_obj.fileUrl hasPrefix:@"http"]){
        // web
        url = [NSURL URLWithString:_obj.fileUrl];
    } else {
        url = [NSURL fileURLWithPath:_obj.fileUrl];
    }

    NSMutableArray *sharingItems = [NSMutableArray new];
    [sharingItems addObject:url];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}

- (void)save{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"loading", nil) maskType:SVProgressHUDMaskTypeBlack];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [MediaObject DownloadToLocal:_obj.fileUrl withCallback:^(NSString * path){
        NSString * docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        [[NSFileManager defaultManager] moveItemAtPath:path toPath:[NSString stringWithFormat:@"%@/%@/%@", docPath, _obj.objType == MediaObjectTypeVideo ? @"video" : @"audio", [path.lastPathComponent stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] error:nil];
        [MediaObject LoadMediaFromLocal];
        
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"load_completed", nil)];
    } withError:^{
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"failed", nil)];
    }];
}

- (void)remove{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"delete_confirm", nil) message:_obj.name delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil) otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
    [alert show];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem * save = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_file_download"] style:UIBarButtonItemStylePlain target:self action:@selector(save)];
    UIBarButtonItem * del = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_delete"] style:UIBarButtonItemStylePlain target:self action:@selector(remove)];
    UIBarButtonItem * share = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_share"] style:UIBarButtonItemStylePlain target:self action:@selector(share)];
    if (_mode == DataModeLocal) {
        self.navigationItem.rightBarButtonItems = @[share, del];
    }
    if (_mode == DataModeRemote) {
        self.navigationItem.rightBarButtonItems = @[share, save];
    }
    
    self.title = _obj.name;
}

- (void)viewWillAppear:(BOOL)animated{
    NSURL *url = nil;
    if([_obj.fileUrl hasPrefix:@"http"]){
        // web
        url = [NSURL URLWithString:_obj.fileUrl];
    } else {
        url = [NSURL fileURLWithPath:_obj.fileUrl];
    }
    _player=[[MPMoviePlayerController alloc] initWithContentURL:url];
    _player.view.frame = self.view.bounds;
    [self.view addSubview:_player.view];
    [_player play];
}

- (void)viewDidDisappear:(BOOL)animated{
    if(_player.playbackState == MPMoviePlaybackStatePlaying){
        [_player stop];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
