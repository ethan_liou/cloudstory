//
//  StartViewController.m
//  CloudStory
//
//  Created by Ethan on 2015/8/23.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import "StartViewController.h"
#import "MediaObject.h"
#import "AppDelegate.h"
#import <SVProgressHUD.h>

@interface StartViewController ()

@property (nonatomic, weak) IBOutlet UIButton * fBtn;
@property (nonatomic, weak) IBOutlet UIButton * sBtn;
@property (nonatomic, weak) IBOutlet UIButton * tBtn;
@property (nonatomic) BOOL connected;

@end

@implementation StartViewController

- (void)connectToSD{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"loading", nil) maskType:SVProgressHUDMaskTypeBlack];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [MediaObject LoadMediaFromLocal];
        _connected = [MediaObject LoadMediaFromRemote];
        dispatch_async(dispatch_get_main_queue(), ^{
            AppDelegate * app = [UIApplication sharedApplication].delegate;
            app.needRefresh = NO;
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            if (_connected) {
                [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"load_completed", nil)];
            } else {
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"connect_warning", nil)];
            }
        });
    });
}

- (void)viewDidLayoutSubviews{
    float left = (_sBtn.bounds.size.width - _sBtn.imageView.bounds.size.width - _sBtn.titleLabel.bounds.size.width - _sBtn.titleEdgeInsets.left) / 2;
    _fBtn.contentEdgeInsets = UIEdgeInsetsMake(0, left, 0, 0);
    _sBtn.contentEdgeInsets = UIEdgeInsetsMake(0, left, 0, 0);
    _tBtn.contentEdgeInsets = UIEdgeInsetsMake(0, left, 0, 0);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_refresh"] style:UIBarButtonItemStylePlain target:self action:@selector(connectToSD)];
}

- (void)viewWillAppear:(BOOL)animated{
    AppDelegate * app = [UIApplication sharedApplication].delegate;
    if(app.needRefresh){
        [self connectToSD];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if(_connected){
        return YES;
    } else {
        [self connectToSD];
        return NO;
    }
}


/*
#pragma mark - Navigation

 
 
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
