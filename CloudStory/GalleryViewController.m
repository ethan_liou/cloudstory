//
//  GalleryViewController.m
//  CloudStory
//
//  Created by Ethan on 2015/4/29.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import "GalleryViewController.h"
#import "MediaObject.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SVProgressHUD.h>

@interface GalleryViewController ()<UIScrollViewDelegate>

@property (nonatomic, weak) IBOutlet UIScrollView * gallery;

@end

@implementation GalleryViewController

#define TAG_PREFIX 10000

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex != alertView.cancelButtonIndex){
        MediaObject * obj = [_objs objectAtIndex:alertView.tag];
        [[NSFileManager defaultManager] removeItemAtPath:obj.fileUrl error:nil];
        [MediaObject LoadMediaFromLocal];
        [_objs removeObjectAtIndex:alertView.tag];
        if(_objs.count > 0){
            _startIdx = alertView.tag;
            [self reload];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)share{
    int page = (int)_gallery.contentOffset.x / (int)_gallery.bounds.size.width;
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    UIImage * image = ((UIImageView *)[_gallery viewWithTag:TAG_PREFIX + page]).image;
    [sharingItems addObject:image];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}

- (void)save{
    int page = (int)_gallery.contentOffset.x / (int)_gallery.bounds.size.width;
    MediaObject * obj = [_objs objectAtIndex:page];
    [SVProgressHUD showWithStatus:NSLocalizedString(@"loading", nil) maskType:SVProgressHUDMaskTypeBlack];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [MediaObject DownloadToLocal:obj.fileUrl withCallback:^(NSString * path){
        NSString * docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        [[NSFileManager defaultManager] moveItemAtPath:path toPath:[NSString stringWithFormat:@"%@/photo/%@", docPath, [path.lastPathComponent stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] error:nil];
        [MediaObject LoadMediaFromLocal];

        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"load_completed", nil)];
    } withError:^{
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"failed", nil)];
    }];
}

- (void)remove{
    int page = (int)_gallery.contentOffset.x / (int)_gallery.bounds.size.width;
    MediaObject * obj = [_objs objectAtIndex:page];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"delete_confirm", nil) message:obj.name delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil) otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
    alert.tag = page;
    [alert show];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem * save = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_file_download"] style:UIBarButtonItemStylePlain target:self action:@selector(save)];
    UIBarButtonItem * del = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_delete"] style:UIBarButtonItemStylePlain target:self action:@selector(remove)];
    UIBarButtonItem * share = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_share"] style:UIBarButtonItemStylePlain target:self action:@selector(share)];
    
    if (_mode == DataModeLocal) {
        self.navigationItem.rightBarButtonItems = @[share, del];
    }
    if (_mode == DataModeRemote) {
        self.navigationItem.rightBarButtonItems = @[share, save];
    }
}

- (void)reload{
    _gallery.contentSize = CGSizeMake(_gallery.bounds.size.width * _objs.count, _gallery.bounds.size.height);
    for(int i = 0 ; i < _objs.count ; i ++){
        MediaObject * obj = [_objs objectAtIndex:i];
        UIImageView * iv = [[UIImageView alloc] initWithFrame:CGRectMake(i * _gallery.bounds.size.width, 0, _gallery.bounds.size.width, _gallery.bounds.size.height)];
        iv.contentMode = UIViewContentModeScaleAspectFit;
        iv.tag = TAG_PREFIX + i;
        if(obj.thumbnailUrl == nil){
            iv.image = [UIImage imageNamed:@"example.jpg"];
        } else if([obj.thumbnailUrl hasPrefix:@"/"]){
            // file
            iv.image = [UIImage imageWithContentsOfFile:obj.fileUrl];
        } else{
            // website
            NSLog(@"%@",obj.fileUrl);
            [iv sd_setImageWithURL:[NSURL URLWithString:obj.fileUrl]];
        }
        [_gallery addSubview:iv];
    }
    [_gallery scrollRectToVisible:CGRectMake(_gallery.bounds.size.width * _startIdx, 0, _gallery.bounds.size.width, _gallery.bounds.size.height) animated:NO];
    if(_startIdx == 0){
        [self scrollViewDidScroll:_gallery];
    }
}

- (void)viewDidLayoutSubviews{
    if(_objs.count > 0)
        [self reload];
}

- (void)viewWillAppear:(BOOL)animated{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    int page = (int)scrollView.contentOffset.x / (int)scrollView.bounds.size.width;
    MediaObject * obj = [_objs objectAtIndex:page];
    self.title = [NSString stringWithFormat:@"%@ (%d/%lu)",obj.name, page + 1, _objs.count];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
