//
//  GalleryViewController.h
//  CloudStory
//
//  Created by Ethan on 2015/4/29.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaObject.h"

@interface GalleryViewController : UIViewController<UIAlertViewDelegate>

@property (nonatomic, strong) NSMutableArray * objs;
@property (nonatomic) NSUInteger startIdx;
@property (nonatomic) DataMode mode;

@end
