//
//  OtherTableViewController.h
//  CloudStory
//
//  Created by Ethan on 2015/8/26.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReloadableDelegate.h"
#import <QuickLook/QuickLook.h>

@interface OtherTableViewController : UIViewController<ReloadableDelegate, UITableViewDataSource, UITableViewDelegate, QLPreviewControllerDataSource, QLPreviewControllerDelegate>

@property (nonatomic, weak) NSNumber * obj_type;

@end
