//
//  PhotoViewController.m
//  CloudStory
//
//  Created by Ethan on 2015/4/26.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import "PhotoViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MediaObject.h"
#import "GalleryViewController.h"
#import "BrowseTabBarViewController.h"
#import <MWPhotoBrowser.h>
#import <SVProgressHUD.h>

@interface PhotoViewController ()<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MWPhotoBrowserDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) NSMutableArray * photos;
@property (nonatomic) NSUInteger selectedIdx;
@property (nonatomic, weak) IBOutlet UICollectionView * grid;
@property (nonatomic, strong) MWPhotoBrowser * browser;

@end

@implementation PhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated{
    [self reloadWithMode:((BrowseTabBarViewController*)self.tabBarController).mode];    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _photos.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    int width = collectionView.bounds.size.width / 3 - 4;
    return CGSizeMake(width, width);
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Image" forIndexPath:indexPath];
    UIImageView * iv = (UIImageView*)[cell viewWithTag:100];
    MediaObject * obj = [_photos objectAtIndex:indexPath.row];
    if(obj.thumbnailUrl == nil){
        iv.image = [UIImage imageNamed:@"example.jpg"];
    } else if([obj.thumbnailUrl hasPrefix:@"/"]){
        // file
        UIImage * image = [UIImage imageWithContentsOfFile:obj.thumbnailUrl];
        if(image.size.height > image.size.width){
            image = [self imageWithImage:image scaledToSize:CGSizeMake(100, 100 * image.size.height / image.size.width)];
        } else {
            image = [self imageWithImage:image scaledToSize:CGSizeMake(100 * image.size.width / image.size.height, 100)];
        }
        iv.image = image;
    } else{
        // website
        [iv sd_setImageWithURL:[NSURL URLWithString:obj.thumbnailUrl]];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    _browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    _browser.displayActionButton = NO;
    _browser.zoomPhotosToFill = YES;
    _browser.enableGrid = NO;
    _browser.alwaysShowControls = YES;
    _browser.remote = ((BrowseTabBarViewController*)self.tabBarController).mode == DataModeRemote;
    [_browser setCurrentPhotoIndex:indexPath.row];
    
    [self.navigationController pushViewController:_browser animated:YES];
}

#pragma mark - Photo

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser{
    return _photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index{
    MediaObject * obj = [_photos objectAtIndex:index];
    NSURL * url = nil;
    if([obj.fileUrl hasPrefix:@"/"]){
        // file
        url = [NSURL fileURLWithPath:obj.fileUrl];
    } else{
        // website
        url = [NSURL URLWithString:obj.fileUrl];
    }
    return [MWPhoto photoWithURL:url];
}


-(void)download{
    MediaObject * obj = [_photos objectAtIndex:_browser.currentIndex];
    [SVProgressHUD showWithStatus:NSLocalizedString(@"loading", nil) maskType:SVProgressHUDMaskTypeBlack];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [MediaObject DownloadToLocal:obj.fileUrl withCallback:^(NSString * path){
        NSString * docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        [[NSFileManager defaultManager] moveItemAtPath:path toPath:[NSString stringWithFormat:@"%@/photo/%@", docPath, [path.lastPathComponent stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] error:nil];
        [MediaObject LoadMediaFromLocal];
        
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"load_completed", nil)];
    } withError:^{
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"failed", nil)];
    }];

}

#define TAG_PREFIX 10000

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex != alertView.cancelButtonIndex){
        MediaObject * obj = [_photos objectAtIndex:alertView.tag];
        [[NSFileManager defaultManager] removeItemAtPath:obj.fileUrl error:nil];
        [MediaObject LoadMediaFromLocal];
        [_photos removeObjectAtIndex:alertView.tag];
        if(_photos.count > 0){
            [_browser reloadData];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}


-(void)del{
    MediaObject * obj = [_photos objectAtIndex:_browser.currentIndex];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"delete_confirm", nil) message:obj.name delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil) otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
    alert.tag = _browser.currentIndex;
    [alert show];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"ImageGallery"]){
        GalleryViewController * vc = (GalleryViewController*)segue.destinationViewController;
        vc.objs = [_photos mutableCopy];
        vc.mode = ((BrowseTabBarViewController*)self.tabBarController).mode;
        vc.startIdx = _selectedIdx;
    }
}

-(void)reloadWithMode:(DataMode)mode{
    _photos = [[MediaObject GetMediaBy:MediaObjectTypePhoto withMode:mode] mutableCopy];
    [_grid reloadData];
}

@end
