//
//  TakeViewController.m
//  CloudStory
//
//  Created by Ethan on 2015/8/23.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import "TakeViewController.h"
#import "MediaObject.h"
#import <SVProgressHUD.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "AppDelegate.h"

@interface TakeViewController ()

@property (nonatomic, strong) NSTimer * timer;
@property (nonatomic, weak) IBOutlet UIImageView * preview;
@property (nonatomic, strong) NSString * lastURL;

@end

@implementation TakeViewController

- (void)getLatestPhoto{
    static BOOL running = NO;
    if(!running){
        [MediaObject GetLatestFile:^(NSString*path){
            if(_lastURL != nil && ![path isEqualToString:_lastURL]){
                AppDelegate * app = [UIApplication sharedApplication].delegate;
                app.needRefresh = YES;
                _preview.hidden = NO;
                [_preview sd_setImageWithURL:[NSURL URLWithString:path]];
            }
            _lastURL = [path copy];
            running = NO;
        }];
    }
}

- (void)downloadToLocal{
    if(!_preview.hidden){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"loading", nil)];
        [MediaObject DownloadToLocal:_lastURL withCallback:^(NSString* cachePath){
            NSString * docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            [[NSFileManager defaultManager] moveItemAtPath:cachePath toPath:[NSString stringWithFormat:@"%@/photo/%@", docPath, [cachePath.lastPathComponent stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] error:nil];
            [MediaObject LoadMediaFromLocal];
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"success", nil)];
        } withError:^{
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"failed", nil)];
        }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_file_download"] style:UIBarButtonItemStylePlain target:self action:@selector(downloadToLocal)];
}

- (void)viewDidAppear:(BOOL)animated{
    [self getLatestPhoto];
    _timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(getLatestPhoto) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [_timer invalidate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
