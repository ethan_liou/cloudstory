//
//  SettingTableViewController.h
//  CloudStory
//
//  Created by Ethan on 2015/8/22.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingTableViewController : UITableViewController<UIAlertViewDelegate, UIActionSheetDelegate>

@end
