//
//  PhotoViewController.h
//  CloudStory
//
//  Created by Ethan on 2015/4/26.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReloadableDelegate.h"

@interface PhotoViewController : UIViewController<ReloadableDelegate>

@end
