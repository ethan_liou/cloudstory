//
//  ReloadableDelegate.h
//  CloudStory
//
//  Created by Ethan on 2015/8/24.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#ifndef CloudStory_ReloadableDelegate_h
#define CloudStory_ReloadableDelegate_h

#import "MediaObject.h"

@protocol ReloadableDelegate<NSObject>

-(void)reloadWithMode:(DataMode)mode;

@end

#endif
