//
//  BrowseTabBarViewController.h
//  CloudStory
//
//  Created by Ethan on 2015/8/23.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaObject.h"

@interface BrowseTabBarViewController : UITabBarController

@property (nonatomic) DataMode mode;

@end
