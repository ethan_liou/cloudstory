//
//  MediaObject.m
//  CloudStory
//
//  Created by Ethan on 2015/4/29.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import "MediaObject.h"
#import <AFNetworking.h>

@interface XMLParserHelper : NSObject<NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * paths;
@property (nonatomic, strong) NSString * prefix;
@property (nonatomic, strong) NSDateFormatter * formatter;

@end

@implementation XMLParserHelper

-(id)initWithPrefix:(NSString *)prefix{
    self = [super init];
    if (self){
        self.paths = [[NSMutableArray alloc] init];
        self.prefix = prefix;
        self.formatter = [[NSDateFormatter alloc] init];
        self.formatter.locale =  [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        self.formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        self.formatter.dateFormat = @"MMM dd HH:mm:ss yyyy";
    }
    return self;
}

- (MediaObjectType)getType:(NSDictionary *)dict{
    MediaObjectType type = MediaObjectTypeOther;
    if([[dict objectForKey:@"type"] integerValue] == 1){
        type = MediaObjectTypeFolder;
    } else{
        NSString * name = [[dict objectForKey:@"name"] lowercaseString];
        if([name hasSuffix:@".jpg"] || [name hasSuffix:@".gif"] || [name hasSuffix:@".png"] || [name hasSuffix:@".bmp"]){
            type = MediaObjectTypePhoto;
        } else if([name hasSuffix:@".3gp"] || [name hasSuffix:@".mp4"] || [name hasSuffix:@".ts"]){
            type = MediaObjectTypeVideo;
        } else if([name hasSuffix:@".mp3"] || [name hasSuffix:@".ogg"] || [name hasSuffix:@".wav"]){
            type = MediaObjectTypeMusic;
        }
    }
    return type;
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if([elementName isEqualToString:@"file"] && ![[attributeDict objectForKey:@"name"] hasPrefix:@"."]){
        MediaObject * obj = [[MediaObject alloc] init];
        obj.objType = [self getType:attributeDict];
        obj.name = [attributeDict objectForKey:@"name"];
        obj.path = [NSString stringWithFormat:@"%@/%@",_prefix, obj.name];
        obj.date = [_formatter dateFromString:[attributeDict objectForKey:@"date"]];
        if(obj.objType == MediaObjectTypePhoto){
            obj.thumbnailUrl = [NSString stringWithFormat:@"http://192.168.1.1/cgi-bin/thumbNail?fn=%@",[obj.path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        }
        obj.fileUrl = [NSString stringWithFormat:@"http://192.168.1.1%@",[[obj.path substringFromIndex:4] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [_paths addObject:obj];
    }
}

@end

@implementation MediaObject

- (id)copyWithZone:(NSZone *)zone{
    MediaObject * newObj = [[MediaObject allocWithZone:zone] init];
    newObj.name = _name;
    newObj.objType = _objType;
    newObj.path = _path;
    newObj.date = [_date copy];
    newObj.thumbnailUrl = _thumbnailUrl;
    newObj.fileUrl = _fileUrl;
    return newObj;
}

-(NSString*)description{
    return [NSString stringWithFormat:@"%@(%d) : %@",_path, _objType, _name];
}

static NSDictionary * remoteObj;
static NSDictionary * localObj;

+(void)getMediaFromRemoteWithPath:(NSString*)root Callback:(void (^)(NSArray*arr))callback{
#ifdef DEBUG
    MediaObject * obj = [[MediaObject alloc] init];
    obj.objType = MediaObjectTypePhoto;
    obj.name = @"Obj1";
    MediaObject * video = [[MediaObject alloc] init];
    video.objType = MediaObjectTypeVideo;
    video.fileUrl = @"http://r5---sn-aigllned.googlevideo.com/videoplayback?lmt=1438480280322324&sver=3&ipbits=0&key=yt5&itag=22&mime=video%2Fmp4&mv=m&mt=1440638016&ms=au&fexp=9407991%2C9408212%2C9408710%2C9409069%2C9415327%2C9415365%2C9415435%2C9415485%2C9416023%2C9416126%2C9416310%2C9416327%2C9416494%2C9417269%2C9417707%2C9418093%2C9418153%2C9418448%2C9419346%2C9420019&dur=4311.040&ratebypass=yes&upn=ftSDIOcT4gY&mn=sn-aigllned&mm=31&source=youtube&initcwndbps=4610000&sparams=dur%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cnh%2Cpl%2Cratebypass%2Csource%2Cupn%2Cexpire&id=o-AEgbWEYv48lxxCkC8Oph0I8rdv6SAZ17xxA0vXdk0stP&ip=2a02%3A2498%3Ae002%3A88%3A91%3A%3A2&pl=47&expire=1440659761&nh=IgpwcjAyLmxocjE0KgkxMjcuMC4wLjE&signature=3E4E8D9F946ED1591ED0E9011F6AC6EFC9C0B8BB.D850A60D7DBBA6BC05F57FA68D66DA80A79A78CB&title=Best+Song+of+Maroon+5+%282015-2016%29";
    video.name = @"Maroon5";
    MediaObject * audio = [[MediaObject alloc] init];
    audio.objType = MediaObjectTypeMusic;
    audio.fileUrl = @"http://www.youtube-mp3.org/get?video_id=KMU0tzLwhbE&ts_create=1440638470&r=MjIwLjEzOS4xNDYuNDc%3D&h2=8171e57c3b82c466b3a6994e9184f53f&s=142490";
    audio.name = @"mp3";
    MediaObject * other = [[MediaObject alloc] init];
    other.objType = MediaObjectTypeOther;
    other.fileUrl = @"http://www.nber.org/papers/w15639.pdf";
    other.name = @"aaa.pdf";
    static NSMutableArray * arr = nil;
    if(arr == nil){
        arr = [[NSMutableArray alloc] initWithObjects:[obj copy], [obj copy], [obj copy], video, audio, other, nil];
    }
    [arr addObject:[obj copy]];
    callback(arr);
#else
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    manager.requestSerializer.timeoutInterval = 5;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/xml"];
    [manager GET:@"http://192.168.1.1/cgi-bin/wifi_filelist"
      parameters:@{@"fn":root}
         success:^(AFHTTPRequestOperation *operation, id responseObject){
             XMLParserHelper * helper = [[XMLParserHelper alloc] initWithPrefix:root];
             NSXMLParser * parser = [[NSXMLParser alloc] initWithData:responseObject];
             parser.delegate = helper;
             [parser parse];
             callback(helper.paths);
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             callback(nil);
         }];
#endif
}

+(NSMutableArray*)generateFilesFromFolder:(NSString*)folder WithType:(MediaObjectType)type{
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSArray * array = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[NSString stringWithFormat:@"%@/%@", path, folder] error:NULL];
    NSMutableArray * output = [[NSMutableArray alloc] initWithCapacity:array.count];
    for(NSString * p in array){
        MediaObject * obj = [[MediaObject alloc] init];
        obj.objType = type;
        obj.name = p;
        obj.thumbnailUrl = [NSString stringWithFormat:@"%@/%@/%@", path, folder, p];
        obj.fileUrl = [NSString stringWithFormat:@"%@/%@/%@", path, folder, p];
        [output addObject:obj];
    }
    return output;
}

+(void)LoadMediaFromLocal{
    localObj = @{
                 [NSNumber numberWithInt:MediaObjectTypePhoto]:[MediaObject generateFilesFromFolder:@"photo" WithType:MediaObjectTypePhoto],
                 [NSNumber numberWithInt:MediaObjectTypeVideo]:[MediaObject generateFilesFromFolder:@"video" WithType:MediaObjectTypeVideo],
                 [NSNumber numberWithInt:MediaObjectTypeMusic]:[MediaObject generateFilesFromFolder:@"audio" WithType:MediaObjectTypeMusic],
                 [NSNumber numberWithInt:MediaObjectTypeOther]:[MediaObject generateFilesFromFolder:@"other" WithType:MediaObjectTypeOther]};
    NSLog(@"Local %@",localObj);
}

+(BOOL)LoadMediaFromRemote{
    __block NSString * root = @"/mnt/sd";
    __block BOOL done = NO;
    __block BOOL ret = YES;
    NSMutableArray * objs = [[NSMutableArray alloc] init];
    while(!done){
        dispatch_semaphore_t sem = dispatch_semaphore_create(0);
        [MediaObject getMediaFromRemoteWithPath:root Callback:^(NSArray *arr) {
            if(arr == nil){
                // it's error
                ret = NO;
                done = YES;
                dispatch_semaphore_signal(sem);
                return;
            }
            [objs addObjectsFromArray:arr];
            done = YES;
            for(MediaObject * obj in objs){
                if(obj.objType == MediaObjectTypeFolder){
                    root = obj.path;
                    [objs removeObject:obj];
                    done = NO;
                    break;
                }
            }
            dispatch_semaphore_signal(sem);
        }];
        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    }
    if(ret){
        remoteObj = @{[NSNumber numberWithInt:MediaObjectTypePhoto]:[[NSMutableArray alloc] init],
                      [NSNumber numberWithInt:MediaObjectTypeVideo]:[[NSMutableArray alloc] init],
                      [NSNumber numberWithInt:MediaObjectTypeMusic]:[[NSMutableArray alloc] init],
                      [NSNumber numberWithInt:MediaObjectTypeOther]:[[NSMutableArray alloc] init]};
        for (MediaObject * obj in objs) {
            [[remoteObj objectForKey:[NSNumber numberWithInt:obj.objType]] addObject:obj];
        }
        for (NSNumber * key in [remoteObj keyEnumerator]) {
            NSMutableArray * arr = [remoteObj objectForKey:key];
            [arr sortUsingComparator:^NSComparisonResult(MediaObject* obj1, MediaObject* obj2) {
                return [obj2.date timeIntervalSinceDate:obj1.date];
            }];
        }
        NSLog(@"%@",remoteObj);
    }
    return ret;
}

+(NSArray*)GetMediaBy:(MediaObjectType)objType withMode:(DataMode)mode{
    NSArray * ret = nil;
    NSDictionary * dict = mode == DataModeRemote ? remoteObj : localObj;
    if(dict != nil){
        ret = [dict objectForKey:[NSNumber numberWithInt:objType]];
    }
    return ret;
}

+(void)getCardInfoWithCallback:(void (^)(NSDictionary* dict))callback{
#ifdef DEBUG
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        sleep(0.5);
        dispatch_async(dispatch_get_main_queue(), ^{
            callback(@{
                       @"Product Name":@"Product",
                       @"Firmware Version":@"Firmware",
                       @"Build Date":@"Build Date",
                       @"IP Address":@"IPAddr",
                       @"MAC Address":@"MACAddr",
                       @"WIFISSID":@"SSID",
                       @"Host WPA2 Switch":@false,
                       @"Host WPA2 Key":@"PASSWORD",
                       @"Channel":@"5"
                       });
        });
    });
#else
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSMutableDictionary * settings = [[NSMutableDictionary alloc] init];
    // GET WIFI
    [manager GET:@"http://192.168.1.1/mtd/config/wsd.conf"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject){
             NSData *data = [[NSData alloc] initWithData:responseObject];
             NSString * str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             NSLog(@"wifi %@",str);
             NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"([a-zA-Z0-9 ]+) : ([a-zA-Z0-9\. ]+)" options:0 error:NULL];
             NSArray * matches = [regex matchesInString:str options:0 range:NSMakeRange(0, str.length)];
             for (NSTextCheckingResult *match in matches) {
                 NSString *key = [str substringWithRange:[match rangeAtIndex:1]];
                 NSString *val = [str substringWithRange:[match rangeAtIndex:2]];
                 [settings setObject:val forKey:key];
             }

             // parse status
             [manager GET:@"http://192.168.1.1/cgi-bin/kcard_status.pl"
               parameters:nil
                  success:^(AFHTTPRequestOperation *operation, id responseObject){
                      NSData *data = [[NSData alloc] initWithData:responseObject];
                      NSString * str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                      NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"<I><b> *([a-zA-Z0-9]+ [a-zA-Z0-9]+)</b></I> : *([a-zA-Z0-9 :_-]+)<br>" options:0 error:NULL];
                      NSArray * matches = [regex matchesInString:str options:0 range:NSMakeRange(0, str.length)];
                      for (NSTextCheckingResult *match in matches) {
                          NSString *key = [str substringWithRange:[match rangeAtIndex:1]];
                          NSString *val = [str substringWithRange:[match rangeAtIndex:2]];
                          [settings setObject:val forKey:key];
                      }
                      // force name as cloud story
                      [settings setObject:@"Cloud Story" forKey:@"Product Name"];
                      callback(settings);
                  }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      NSLog(@"Error: %@", error);
                  }];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
#endif
}

+(void)GetLatestFile:(void (^)(NSString* path))callback{
#ifdef DEBUG
    static int i = 0;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        sleep(0.5);
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString * output = @"";
            if(++i % 2 == 0){
                output = @"http://www.google.com/doodle4google/images/d4g_logo_global.jpg";
            }
            callback(output);
        });
    });
#else
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:@"http://192.168.1.1/cgi-bin/wifi_get_newest_file"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject){
             NSData *data = [[NSData alloc] initWithData:responseObject];
             NSString * str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@" src=\"([^\"]+)\" " options:0 error:NULL];
             NSArray * matches = [regex matchesInString:str options:0 range:NSMakeRange(0, str.length)];
             NSString * path = @"";
             for (NSTextCheckingResult *match in matches) {
                 path = [str substringWithRange:[match rangeAtIndex:1]];
             }
             if([path hasPrefix:@"/sd"]){
                 path = [path stringByReplacingOccurrencesOfString:@"/._" withString:@"/"];
                 callback([NSString stringWithFormat:@"http://192.168.1.1%@",[path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]);
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
#endif
}

+(void)DownloadToLocal:(NSString*)url withCallback:(void (^)(NSString* path))callback withError:(void (^)())errCallback{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:url
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             // save To Cache
             NSString *path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
             NSString * filePath = [NSString stringWithFormat:@"%@/%@", path, url.lastPathComponent];
             NSData *data = [[NSData alloc] initWithData:responseObject];
             [data writeToFile:filePath atomically:YES];
             NSLog(@"Download success %@", filePath);
             callback(filePath);
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"%@",error);
             errCallback();
         }];
}

+(void)saveSetting:(NSDictionary*)dict withCallback:(void (^)())callback{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:@"http://192.168.1.1/cgi-bin/kcard_save_config.pl"
      parameters:dict
         success:^(AFHTTPRequestOperation *operation, id responseObject){
             NSData *data = [[NSData alloc] initWithData:responseObject];
             NSString * str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             NSLog(@"%@",str);
             callback();
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
}

@end
