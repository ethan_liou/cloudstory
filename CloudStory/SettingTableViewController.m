//
//  SettingTableViewController.m
//  CloudStory
//
//  Created by Ethan on 2015/8/22.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import "SettingTableViewController.h"
#import "MediaObject.h"
#import <SVProgressHUD.h>

@interface SettingTableViewController ()

@property (nonatomic, strong) NSArray * firmware;
@property (nonatomic, strong) NSArray * wifi;
@property (nonatomic, strong) NSArray * settings;

@end

typedef NS_ENUM(NSUInteger, SettingType) {
    SETTINGTYPE_INFO,
    SETTINGTYPE_STRING,
    SETTINGTYPE_BOOLEAN,
    SETTINGTYPE_LIST
};

@interface Setting : NSObject

@property (nonatomic, strong) NSString * key;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) id val;
@property (nonatomic, strong) id info;
@property (nonatomic) SettingType type;

-(id)initWithKey:(NSString*)key withTitle:(NSString*)title withType:(SettingType)type;
+(id)settingWithKey:(NSString*)key withTitle:(NSString*)title withType:(SettingType)type;

@end

@implementation Setting

-(id)initWithKey:(NSString*)key withTitle:(NSString*)title withType:(SettingType)type{
    self = [super init];
    if(self){
        _title = title;
        _key = key;
        _type = type;
    }
    return self;
}

+(id)settingWithKey:(NSString*)key withTitle:(NSString*)title withType:(SettingType)type{
    return [[Setting alloc] initWithKey:key withTitle:title withType:type];
}

@end

@implementation SettingTableViewController

-(NSDictionary*)settingDict{
    return @{
             @"Login_Enable":@"No",
             @"Auto_WIFI":@"Yes",
             @"Local_IP1":@"1",
             @"Local_IP2":@"1",
             @"Sender_IP1":@"1",
             @"Sender_IP2":@"1",
             @"WIFI_SSID":((Setting*)[_wifi objectAtIndex:0]).val,
             @"ctr":[((Setting*)[_wifi objectAtIndex:1]).val boolValue] ? @"on" : @"",
             @"Host_WPA_KEY":[((Setting*)[_wifi objectAtIndex:1]).val boolValue] ? ((Setting*)[_wifi objectAtIndex:2]).val : @"",
             @"Channel_Num":((Setting*)[_wifi objectAtIndex:3]).val
             };
}


- (void)changeValue:(id)sender{
    UISwitch * s = sender;
    ((Setting*)[_wifi objectAtIndex:s.tag]).val = [NSNumber numberWithBool:s.on];
    NSLog(@"Bool %@",[self settingDict]);
}

- (void)saveSetting{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"loading", nil)];
    [MediaObject saveSetting:[self settingDict] withCallback:^{
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"restart", nil)];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"setting", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_save"] style:UIBarButtonItemStylePlain target:self action:@selector(saveSetting)];
    [SVProgressHUD showWithStatus:NSLocalizedString(@"loading", nil)];
    [MediaObject getCardInfoWithCallback:^(NSDictionary*dict){
        [SVProgressHUD dismiss];
        _firmware = @[
                      [Setting settingWithKey:@"Product Name" withTitle:NSLocalizedString(@"product_name", nil) withType:SETTINGTYPE_INFO],
                      [Setting settingWithKey:@"Firmware Version" withTitle:NSLocalizedString(@"firmware_ver", nil) withType:SETTINGTYPE_INFO],
                      [Setting settingWithKey:@"Build Date" withTitle:NSLocalizedString(@"build_date", nil)  withType:SETTINGTYPE_INFO] ,
                      [Setting settingWithKey:@"My IP Addr" withTitle:NSLocalizedString(@"ip_address", nil)  withType:SETTINGTYPE_INFO],
                      [Setting settingWithKey:@"MAC Address" withTitle:NSLocalizedString(@"mac_address", nil)  withType:SETTINGTYPE_INFO],
                      ];
        Setting * channel = [Setting settingWithKey:@"Channel" withTitle:NSLocalizedString(@"channel", nil) withType:SETTINGTYPE_LIST];
        channel.info = @[@"auto",@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11"];
        _wifi = @[
                  [Setting settingWithKey:@"WIFISSID" withTitle:NSLocalizedString(@"wifi_ssid", nil) withType:SETTINGTYPE_STRING],
                  [Setting settingWithKey:@"Host WPA2 Switch" withTitle:NSLocalizedString(@"encrypted", nil) withType:SETTINGTYPE_BOOLEAN],
                  [Setting settingWithKey:@"Host WPA2 Key" withTitle:NSLocalizedString(@"password", nil) withType:SETTINGTYPE_STRING],
                  channel
                  ];
        for (Setting * setting in _firmware) {
            setting.val = [dict objectForKey:setting.key];
        }
        for (Setting * setting in _wifi) {
            if([setting.key isEqualToString:@"Host WPA2 Switch"]){
                setting.val = [NSNumber numberWithBool:[dict objectForKey:setting.key] != nil && ([[dict objectForKey:setting.key] isEqualToString:@"on"] || [[dict objectForKey:setting.key] isEqualToString:@"true"])];
            } else {
                setting.val = [dict objectForKey:setting.key];
            }
        }
        NSLog(@"%@",[self settingDict]);
        _settings = @[_firmware, _wifi];
        [self.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _settings.count;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return section == 0 ? NSLocalizedString(@"product_info", nil) : NSLocalizedString(@"wifi_setting", nil);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[_settings objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * IDENTIFIER = @"SettingCell";
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:IDENTIFIER];
    
    Setting * setting = [[_settings objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = setting.title;
    switch (setting.type) {
        case SETTINGTYPE_BOOLEAN:
        {
            UISwitch *switchview = [[UISwitch alloc] initWithFrame:CGRectZero];
            switchview.on = [setting.val boolValue];
            switchview.tag = indexPath.row;
            cell.accessoryView = switchview;
            [switchview addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventValueChanged];
            break;
        }
        default:
            cell.detailTextLabel.text = setting.val;
            break;
    }
    
    return cell;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex != alertView.cancelButtonIndex){
        Setting * setting = [_wifi objectAtIndex:alertView.tag];
        setting.val = [alertView textFieldAtIndex:0].text;
        [self.tableView reloadData];
        NSLog(@"Str %@", [self settingDict]);
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Setting * setting = [[_settings objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    if(setting.type == SETTINGTYPE_STRING){
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:setting.title
                                                         message:@""
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                               otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        alert.tag = indexPath.row;
        UITextField *tf=[alert textFieldAtIndex:0];
        tf.text = setting.val == nil ? @"" : setting.val;
        [alert show];
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    } else if(setting.type == SETTINGTYPE_LIST){
        UIActionSheet * sheet = [[UIActionSheet alloc] initWithTitle:setting.title delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil) destructiveButtonTitle:nil otherButtonTitles:nil];
        sheet.tag = indexPath.row;
        for(NSString * title in setting.info){
            [sheet addButtonWithTitle:title];
        }
        [sheet showInView:self.view];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex != actionSheet.cancelButtonIndex){
        Setting * setting = [_wifi objectAtIndex:actionSheet.tag];
        setting.val = [setting.info objectAtIndex:buttonIndex - 1];
        NSLog(@"List %@", [self settingDict]);        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
