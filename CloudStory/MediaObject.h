//
//  MediaObject.h
//  CloudStory
//
//  Created by Ethan on 2015/4/29.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(int, MediaObjectType) {
    MediaObjectTypeFolder = 0,
    MediaObjectTypePhoto,
    MediaObjectTypeVideo,
    MediaObjectTypeMusic,
    MediaObjectTypeOther
};

typedef NS_ENUM(int, DataMode) {
    DataModeLocal,
    DataModeRemote
};

@interface MediaObject : NSObject

@property (nonatomic) MediaObjectType objType;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * path;
@property (nonatomic, strong) NSDate * date;
@property (nonatomic, strong) NSString * thumbnailUrl;
@property (nonatomic, strong) NSString * fileUrl;
@property (nonatomic, strong) NSString * cachePath;

+(void)LoadMediaFromLocal;
+(BOOL)LoadMediaFromRemote;
+(NSArray*)GetMediaBy:(MediaObjectType)objType withMode:(DataMode)mode;
+(void)getCardInfoWithCallback:(void (^)(NSDictionary* dict))callback;
+(void)GetLatestFile:(void (^)(NSString* path))callback;
+(void)DownloadToLocal:(NSString*)url withCallback:(void (^)(NSString* path))callback withError:(void (^)())errCallback;
+(void)saveSetting:(NSDictionary*)dict withCallback:(void (^)())callback;

@end
