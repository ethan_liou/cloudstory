//
//  BrowseTabBarViewController.m
//  CloudStory
//
//  Created by Ethan on 2015/8/23.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import "BrowseTabBarViewController.h"
#import <SVProgressHUD.h>
#import "ReloadableDelegate.h"

@interface BrowseTabBarViewController ()

@end

@implementation BrowseTabBarViewController

- (void)refresh{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"loading", nil) maskType:SVProgressHUDMaskTypeBlack];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [MediaObject LoadMediaFromLocal];
        BOOL ret = [MediaObject LoadMediaFromRemote];
        dispatch_async(dispatch_get_main_queue(), ^{
            id<ReloadableDelegate> obj = (id<ReloadableDelegate>)self.selectedViewController;
            [obj reloadWithMode:_mode];
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            if (ret) {
                [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"load_completed", nil)];
            } else {
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"failed", nil)];
            }
        });
    });
}

- (void)local{
    _mode = DataModeLocal;
    id<ReloadableDelegate> obj = (id<ReloadableDelegate>)self.selectedViewController;
    [obj reloadWithMode:_mode];
    [self resetBar];
}

- (void)remote{
    _mode = DataModeRemote;
    id<ReloadableDelegate> obj = (id<ReloadableDelegate>)self.selectedViewController;
    [obj reloadWithMode:_mode];
    [self resetBar];
}

- (void)resetBar{
    UIBarButtonItem * refresh = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_refresh"] style:UIBarButtonItemStylePlain target:self action:@selector(refresh)];
    UIBarButtonItem * local = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_phone_iphone"] style:UIBarButtonItemStylePlain target:self action:@selector(local)];
    UIBarButtonItem * remote = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_sd_card"] style:UIBarButtonItemStylePlain target:self action:@selector(remote)];

    if (_mode == DataModeLocal) {
        self.navigationItem.rightBarButtonItems = @[refresh, remote];
    }
    if (_mode == DataModeRemote) {
        self.navigationItem.rightBarButtonItems = @[refresh, local];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _mode = DataModeRemote;
    self.title = NSLocalizedString(@"browse", nil);
    [self resetBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
