//
//  OtherTableViewController.m
//  CloudStory
//
//  Created by Ethan on 2015/8/26.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import "OtherTableViewController.h"
#import "MediaObject.h"
#import "BrowseTabBarViewController.h"
#import "DetailViewController.h"
#import <SVProgressHUD.h>
@import MediaPlayer;

@interface OtherTableViewController ()

@property (nonatomic, weak) NSArray * objs;
@property (nonatomic, weak) IBOutlet UITableView * tableView;

@end

@implementation OtherTableViewController

-(void)reloadWithMode:(DataMode)mode{
    _objs = [MediaObject GetMediaBy:(MediaObjectType)_obj_type.integerValue withMode:mode];
    NSLog(@"%@",_objs);
    [_tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated{
    [self reloadWithMode:((BrowseTabBarViewController*)self.tabBarController).mode];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _objs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * ID = @"CELL";
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    MediaObject * obj = [_objs objectAtIndex:indexPath.row];
//    if(obj.objType == MediaObjectTypeOther){
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    } else {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
//    }
    cell.textLabel.text = obj.name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MediaObject * obj = [_objs objectAtIndex:indexPath.row];
    if(obj.objType != MediaObjectTypeOther){
        [self performSegueWithIdentifier:@"detail" sender:obj];
    } else {
        if([obj.fileUrl hasPrefix:@"http"]){
            [SVProgressHUD showWithStatus:NSLocalizedString(@"loading", nil) maskType:SVProgressHUDMaskTypeBlack];
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            [MediaObject DownloadToLocal:obj.fileUrl withCallback:^(NSString * path){
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                [SVProgressHUD dismiss];
                obj.cachePath = path;
                QLPreviewController * controller = [[QLPreviewController alloc] init];
                controller.view.tag = indexPath.row;
                controller.dataSource = self;
                [self.navigationController pushViewController:controller animated:YES];
            } withError:^{
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"failed", nil)];
            }];
        } else {
            obj.cachePath = obj.fileUrl;
            QLPreviewController * controller = [[QLPreviewController alloc] init];
            controller.view.tag = indexPath.row;
            controller.dataSource = self;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    DetailViewController * vc = (DetailViewController*)segue.destinationViewController;
    vc.obj = sender;
    vc.mode = ((BrowseTabBarViewController*)self.tabBarController).mode;
}

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller{
    return 1;
}

- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index{
    MediaObject * obj = [_objs objectAtIndex:controller.view.tag];
    NSLog(@"%@",obj.cachePath);
    return [NSURL fileURLWithPath:obj.cachePath];
}


//#pragma mark - UIDocumentInteractionControllerDelegate
//
//- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
//    
//    return  self;
//}
//
//- (void)documentInteractionController:(UIDocumentInteractionController *)controller willBeginSendingToApplication:(NSString *)application {
//    
//    NSLog(@"Starting to send this puppy to %@", application);
//}
//
//- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application {
//    
//    NSLog(@"We're done sending the document.");
//}

@end
