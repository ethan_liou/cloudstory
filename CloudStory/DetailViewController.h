//
//  DetailViewController.h
//  CloudStory
//
//  Created by Ethan on 2015/8/26.
//  Copyright (c) 2015年 globemaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaObject.h"

@interface DetailViewController : UIViewController

@property (nonatomic, weak) MediaObject * obj;
@property (nonatomic) DataMode mode;

@end
